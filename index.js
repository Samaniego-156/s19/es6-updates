// Create variable getCube and use the exponent operator to compute for the cube of a number.
// Using template literals, print out the value of the getCube variable with a message of "The cube of <num> is... "

let num = 8;
const getCube = num ** 3;
console.log(`The cube of ${num} is ${getCube}.`);

// Create a variable address with a value of an array containing details of an address
// Destructure the array and print out a messasge with the full address using Template Literals

let address = ['258', 'Washington Ave NW', 'California', '90011'];

let [housenum, street, state, zip] = address;

console.log(`I live at ${housenum} ${street}, ${state} ${zip}!`);

// Create a variable animal with a value of ann object data type with different animal details as it's properties
// Destructure the object and print out a mesage with the details of the animal using template literals

let animal = {
	name: 'Po',
	type: 'panda',
	weight: '120kg',
	location: 'Valley of Peace'
};

let {name, type, weight} = animal;

console.log(`${name}, a clumsy ${type}, weighing approximately ${weight}, is a kung fu fanatic who lives in the ${location} and works in his goose father Mr. Ping's noodle shop.`)


// Create an array of numbers
// Loop through the array using forEach, an arrow function and using the implicit return statement to rpint out the numbers


let add = [1, 2, 3, 4, 5]
let total = 0;

add.forEach(sum => {
	console.log(sum);});


// Create a variable reducenumber andusing the reduce array method and an arrow function console log the sum of all the numbers in the array.

const reduceNumber = (addx, addy) => addx + addy;
console.log(add.reduce(reduceNumber));

// Create a class of a Dog and a constructor that will accept a name, age, and breed as it's properties
// Create/instantiate a new object from the class Dog and console log the object
class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog('Patrasche', 5, 'Bouvier des Flandres');
console.log(myDog);
